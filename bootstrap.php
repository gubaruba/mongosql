<?php
require_once('Autoloader.php');

AutoLoader::registerDirectory('application');
spl_autoload_register(['Autoloader', 'loadClass']);