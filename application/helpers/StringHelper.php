<?php
namespace application\helpers;

/**
 * Class StringHelper
 *
 * Provides the set of methods for working with strings
 *
 * @package application\helpers
 */
class StringHelper
{
    /**
     * Check if string starts with passed substring
     *
     * @param string $haystack
     * @param string $needle
     * @return boolean true if starts
     */
    public static function stringStartsWith($haystack, $needle)
    {
        return substr($haystack, 0, strlen($needle)) == $needle;
    }

    /**
     * Casts string containing number to number format
     *
     * @param string $val
     * @return integer|float|string number if string contains number or string if no
     */
    public static function castType($val)
    {
        if (is_numeric($val)) {
            return $val + 0;
        }
        return $val;
    }
}