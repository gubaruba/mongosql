<?php
namespace application;

/**
 * Interface IQuery
 *
 * Declares interface that should be implemented in all query classes
 *
 * @package application
 * @author Volodymyr Lalykin
 */
interface IQuery
{
    /**
     * Sets MongoDB projection according to passed MySQL SELECT expression
     *
     * @param string $expression
     * @throws \Exception if expression does not match SELECT clause pattern
     */
    public function setProjection($expression);

    /**
     * Gets MongoDB projection
     *
     * @return array
     */
    public function getProjection();

    /**
     * Sets MongoDB collection according to passed MySQL FROM expression
     *
     * @param string $expression
     * @throws \Exception if expression does not match FROM clause pattern
     */
    public function setCollection($expression);

    /**
     * Gets MongoDB collection
     *
     * @return array
     */
    public function getCollection();

    /**
     * Sets MongoDB condition according to passed MySQL WHERE expression
     *
     * @param string $expression
     * @throws \Exception if expression does not match WHERE clause pattern
     */
    public function setCondition($expression);

    /**
     * Gets MongoDB condition
     *
     * @return array
     */
    public function getCondition();

    /**
     * Handles GROUP By MySQL clause
     *
     * @param string $expression
     * @throws \Exception not yet implented
     */
    public function setGroup($expression);

    /**
     * Gets MongoDB group
     *
     * @return array
     */
    public function getGroup();

    /**
     * Sets MongoDB sort according to passed MySQL ORDER BY expression
     *
     * @param string $expression
     * @throws \Exception if expression does not match ORDER BY clause pattern
     */
    public function setSort($expression);

    /**
     * @inheritdoc
     */
    public function getSort();

    /**
     * Sets MongoDB skip according to passed SKIP expression
     *
     * @param string $expression
     * @throws \Exception if expression does not match SKIP clause pattern
     */
    public function setSkip($expression);

    /**
     * Gets MongoDB skip
     *
     * @return integer
     */
    public function getSkip();

    /**
     * Sets MongoDB limit according to passed MySQL LIMIT expression
     *
     * @param string $expression
     * @throws \Exception if expression does not match LIMIT clause pattern
     */
    public function setLimit($expression);

    /**
     * Gets MongoDB limit
     *
     * @return integer
     */
    public function getLimit();

    /**
     * Executes query
     *
     * @param \MongoDB $db MongoDB DB object
     * @param array $input
     * @return mixed
     */
    public function execute(\MongoDB $db, $input);
}