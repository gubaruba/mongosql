<?php
namespace application\mysql;

use application\helpers\StringHelper;
use application\IQuery;
use Exception;
use MongoDB;
use MongoCursor;
use MongoCollection;

/**
 * Class Query
 *
 * Contains a set of methods for preparing and running MongoDB queries passed in MySQL style
 *
 * @package application\mysql
 * @author Volodymyr Lalykin <lalykinvv@gmail.com>
 */
class Query implements IQuery
{
    /**
     * @var array
     */
    private $projection = [];

    /**
     * @var string
     */
    private $collection;

    /**
     * @var array
     */
    private $condition = [];

    /**
     * @var array
     */
    private $group = [];

    /**
     * @var array
     */
    private $sort = [];

    /**
     * @var integer
     */
    private $skip = 0;

    /**
     * @var integer
     */
    private $limit = 0;

    /**
     * @var array
     */
    public static $operatorMap = [
        '=' => '$eq',
        '<>' => '$ne',
        '<=' => '$lte',
        '<' => '$lt',
        '>=' => '$gte',
        '>' => '$gt',
    ];

    /**
     * @var array
     */
    public static $logicalOperators = ['and', 'xor', 'or'];

    /**
     * @var string query string pattern
     */
    private static $pattern = '#^(SELECT\s.+)(\s+FROM\s\w+)(\s+WHERE\s.+)?(\s+GROUP BY\s.+)?(\s+ORDER BY\s.+)?(\s+SKIP\s+\d+)?(\s+LIMIT\s+\d+)?$#iU';

    /**
     * Returns a result of initial validation of entered query string
     *
     * @param string $input
     *
     * @return boolean whether query string is valid
     */
    private function validate($input)
    {
        return preg_match(self::$pattern, $input);
    }

    /**
     * Parses entered query string and assigns properties which are relevant to MySQL operators
     *
     * @param string $input
     * @throws Exception on error
     */
    private function parseExpressions($input)
    {
        preg_match(self::$pattern, $input, $matches);

        try {
            $this->setProjection($matches[1]);

            $this->setCollection($matches[2]);

            if ($where = $this->getClause('where', $matches)) {
                $this->setCondition($where);
            }

            if ($group = $this->getClause('group by', $matches)) {
                $this->setGroup($group);
            }

            if ($sort = $this->getClause('order by', $matches)) {
                $this->setSort($sort);
            }

            if ($skip = $this->getClause('skip', $matches)) {
                $this->setSkip($skip);
            }

            if ($limit = $this->getClause('limit', $matches)) {
                $this->setLimit($limit);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Returns appropriate item from matched strings by clause name
     *
     * @param string $clause
     * @param string[] $matches
     * @return boolean|string false if not found
     */
    private function getClause($clause, $matches)
    {
        foreach ($matches as $match) {
            $lowerCaseMath = trim(strtolower($match));
            if (StringHelper::stringStartsWith($lowerCaseMath, $clause)) {
                return $match;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function setProjection($expression)
    {
        if (!preg_match('#^SELECT\s+(\*|\w+(\.?\w+|\.\*)*(,\s?\w+(\.?\w+|\.\*)?)*)$#i', trim($expression), $matches)) {
            throw new Exception('You have an error in SQL syntax; check the right syntax to use near SELECT');
        }

        $projection = preg_split('#,\s*#', $matches[1]);
        $this->projection = [];
        foreach ($projection as $item) {
            if ($item == '*') {
                $this->projection = [];
                break;
            } elseif (preg_match('#^\w+(\.{1}\w+)*\.\*$#', $item, $matches)) {
                $this->projection[] = substr($item, 0, -2);
            } else {
                $this->projection[] = $item;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getProjection()
    {
        return $this->projection;
    }

    /**
     * @inheritdoc
     */
    public function setCollection($expression)
    {
        if (!preg_match('#^FROM\s+(\w+)$#i', trim($expression), $matches)) {
            throw new Exception('You have an error in SQL syntax; check the right syntax to use near FROM');
        }

        $this->collection = $matches[1];
    }

    /**
     * @inheritdoc
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @inheritdoc
     */
    public function setCondition($expression)
    {
        //Encode quoted strings to prevent further string separation by whitespaces
        foreach ($this->getQuotedStrings($expression) as $string) {
            $expression = str_replace('"' . $string . '"', urlencode($string), $expression);
        }

        if (!preg_match('#^WHERE\s+((\w+(\.?\w+)?\s*(=|<>|<=|>=|>|<){1}\s*\S+)(\s+(AND|OR|XOR)\s+(\w+(\.?\w+)?\s*(=|<>|<|>|<=|>=){1}\s*\S+))*)$#i', trim($expression), $matches)) {
            throw new Exception('You have an error in SQL syntax; check the right syntax to use near WHERE');
        }

        $parts = preg_split('#\s+#', $matches[1]);

        $newExpression = '';
        foreach ($parts as $part) {
            $part = trim($part);
            $newExpression .= in_array(strtolower($part), self::$logicalOperators) ? ' ' . strtolower($part) . ' ' : $part;
        }

        $this->condition = $this->generateCondition($newExpression);
    }

    /**
     * @inheritdoc
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @inheritdoc
     */
    public function setGroup($expression)
    {
        throw new Exception('Group expression is not yet implemented');
    }

    /**
     * @inheritdoc
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @inheritdoc
     */
    public function setSort($expression)
    {
        if (!preg_match('#^ORDER BY\s+((\w+(\.?\w+)?(?:\s+(?:ASC|DESC))?)(?:,\s*(\w+(\.?\w+)?(?:\s+(?:ASC|DESC))?))*)$#i', trim($expression), $matches)) {
            throw new Exception('You have an error in SQL syntax; check the right syntax to use near ORDER BY');
        }

        $this->sort = [];
        $sort = preg_split('#,\s*#', $matches[1]);

        foreach ($sort as $key => $item) {
            $sort[$key] = preg_split('#\s+#', $item);
            $this->sort[$sort[$key][0]] = (isset($sort[$key][1])) ? (strtolower($sort[$key][1]) == 'desc' ? -1 : 1) : 1;
        }
    }

    /**
     * @inheritdoc
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @inheritdoc
     */
    public function setSkip($expression)
    {
        if (!preg_match('#^SKIP\s+(\d+)$#i', trim($expression), $matches)) {
            throw new Exception('You have an error in SQL syntax; check the right syntax to use near SKIP');
        }

        $this->skip = $matches[1];
    }

    /**
     * @inheritdoc
     */
    public function getSkip()
    {
        return $this->skip;
    }

    /**
     * @inheritdoc
     */
    public function setLimit($expression)
    {
        if (!preg_match('#^LIMIT\s+(\d+)$#i', trim($expression), $matches)) {
            throw new Exception('You have an error in SQL syntax; check the right syntax to use near SKIP');
        }

        $this->limit = $matches[1];
    }

    /**
     * @inheritdoc
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     *  Returnes an array of strings which were quoted in passed string
     *
     * @param string $string
     * @return array
     */
    private function getQuotedStrings($string)
    {
        $data = explode('"', $string);
        $result = [];

        foreach ($data as $key => $item) {
            if ($key % 2 != 0) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * Generates MongoDB condition
     *
     * @param string $expression e.g.
     * ```php
     *      email = test@example.com AND age<=30 XOR level <> 2 OR name="Ivan Ivanov" OR name="Petr Petrov"
     * ```
     *
     * @return array
     */
    private function generateCondition($expression)
    {
        $condition = $this->updateByComparisonOperator(explode(' ', $expression));
        foreach (self::$logicalOperators as $operator) {
            $condition = $this->updateByOperator($condition, $operator);
        }
        return $condition[0];
    }

    /**
     * Returns condition array where comparisons represented in MongoDB style. E.g.
     *
     * ```php
     *      [
     *          'a=1',
     *          'and',
     *          'b<=2',
     *          'or',
     *          'c>3',
     *      ]
     * ```
     * becomes
     *
     * ```php
     *      [
     *          ['a' => ['$eq' => 1]],
     *          'and',
     *          ['b' => ['$lte' => 2]],
     *          'or',
     *          ['c' => ['$gt' => 3]],
     *      ]
     * ```
     *
     * @param array $condition
     * @return array
     */
    private function updateByComparisonOperator($condition)
    {
        foreach ($condition as $key => $part) {
            if (preg_match('#^(\w+(?:\.?\w+)?)\s*(=|<>|<=|>=|>|<){1}\s*(\S+)$#', $part, $m)) {
                $condition[$key] = [$m[1] => [self::$operatorMap[$m[2]] => StringHelper::castType(urldecode($m[3]))]];
            }
        }
        return $condition;
    }

    /**
     * Recursive method which handles and, or, xor operators in passed array. E.g. array
     *
     * ```php
     *      [
     *          ['a' => ['$eq' => 1]],
     *          'and',
     *          ['b' => ['$lte' => 2]],
     *          'or',
     *          ['c' => ['$gt' => 3]],
     *      ]
     * ```
     *
     * becomes
     *
     * ```php
     *      [
     *          '$or' => [
     *              [
     *                  '$and' => [
     *                      ['a' => ['$eq' => 1]],
     *                      ['b' => ['$lte' => 2]],
     *                  ]
     *              ]
     *          ]
     *      ]
     * ```
     *
     * @param array $condition
     * @param string $operator and, or or xor
     *
     * @return array
     */
    private function updateByOperator($condition, $operator)
    {
        foreach ($condition as $key => $item) {
            if ($item == $operator) {
                if ($item == 'xor') {
                    $condition[$key] = $this->xorFunc($condition[$key - 1], $condition[$key + 1]);
                } else {
                    $condition[$key] = [
                        '$' . $operator => [
                            $condition[$key - 1],
                            $condition[$key + 1],
                        ]
                    ];
                }
                unset($condition[$key - 1]);
                unset($condition[$key + 1]);
                return $this->updateByOperator(array_values($condition), $operator);
            }
        }
        return array_values($condition);
    }

    /**
     * Own implementation of XOR function
     * XOR = a && !b || b && !a
     *
     * @param $a
     * @param $b
     * @return array
     */
    private function xorFunc($a, $b)
    {
        return [
            '$or' => [
                [
                    '$and' => [
                        $a,
                        ['$nor' => [$b]]
                    ]
                ],
                [
                    '$and' => [
                        $b,
                        ['$nor' => [$a]]
                    ]
                ],
            ]
        ];
    }

    /**
     * Runs query and returns the result
     * @param MongoDB $db
     *
     * @return array
     * @throws Exception on error
     */
    private function run($db)
    {
        try {
            $collection = new MongoCollection($db, $this->getCollection());
            $cursor = $collection->find($this->getCondition(), $this->getProjection())->sort($this->getSort())->skip($this->getSkip())->limit($this->getLimit());
            return iterator_to_array($cursor);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param MongoDB $db
     * @param string $input query string passed through command line
     * @return MongoCursor query result
     * @throws Exception if got errors
     */
    public function execute(MongoDB $db, $input)
    {
        if (!$this->validate($input)) {
            throw new Exception('Wrong query string format');
        }

        try {
            $this->parseExpressions($input);
            return $this->run($db);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
