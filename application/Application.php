<?php
namespace application;

use InvalidArgumentException;
use MongoClient;
use MongoConnectionException;
use MongoDB;
use Exception;

/**
 * Class Application
 *
 * Provides a set of methods for initializing CLI application for querying MongoDB data in other DBs way
 *
 * @package application
 * @author Volodymyr Lalykin
 */
class Application
{
    /**
     * @var string
     */
    public $server = 'mongodb://localhost:27017';

    /**
     * @var string
     */
    public $dbname;

    /**
     * @var MongoDB db object.
     */
    private $db;

    /**
     * Contains concrete DB query object
     *
     * @var IQuery
     */
    private $query;

    /**
     * Application constructor.
     *
     * @param array $config e.g
     *
     * ```php
     *      [
     *      'server' => 'mongodb://localhost:27017',
     *      'dbname' => 'test_db'
     *      ]
     * ```
     *
     */
    public function __construct($config)
    {
        $this->configure($config);
        $this->createConnection();
    }


    /**
     * Query setter
     *
     * @param IQuery $query
     */
    public function setQuery(IQuery $query)
    {
        $this->query = $query;
    }

    /**
     * Query getter
     *
     * @return IQuery
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * DB setter
     *
     * @param MongoDB $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * DB getter
     *
     * @return MongoDB
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Reads application key => value configuration and set appropriate properties
     *
     * @param array $config
     *
     * @return boolean
     * @throws InvalidArgumentException if config is not an array
     */
    public function configure($config)
    {
        if (!is_array($config)) {
             throw new InvalidArgumentException('Application config must be an array');
        }

        foreach ($config as $name => $property) {
            $this->$name = $property;
        }
    }


    private function recursive($array, $showId = false)
    {
        foreach($array as $key => $value){
            if (!$showId && $key == '_id') {
                continue;
            }
            if(is_array($value)){
                $this->recursive($value, $showId);
                echo "\n";
            } else{
                echo "$key: $value\n";
            }
        }
    }

    /**
     * Creates new MongoDB connection
     *
     * @throws InvalidArgumentException if could not set required params
     * @throws MongoConnectionException if connection fails
     */
    private function createConnection()
    {
        if (!$this->server) {
            throw new InvalidArgumentException('"server" parameter cannot be empty. Check your config.');
        }

        if (!$this->dbname) {
            throw new InvalidArgumentException('"dbname" parameter cannot be empty. Check your config.');
        }

        try {
            $connection = new MongoClient($this->server);
            $this->setDb($connection->selectDB($this->dbname));
        } catch (MongoConnectionException $e) {
            throw new MongoConnectionException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Runs application
     *
     * @param array $input
     * @return mixed
     */
    public function run($input)
    {
        try {
            $result = $this->query->execute($this->getDb(), $input);
            $this->recursive($result);
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage(). "\n";
        }
    }
}