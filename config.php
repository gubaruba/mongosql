<?php
return [
    'server' => 'mongodb://localhost:27017',
    'dbname' => 'test_db',
    'test_dbname' => 'test',
    'test_collection' => 'test_collection'
];