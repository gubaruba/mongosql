# Зависимости #

* PHP >= 5.6
* MongoDB >= 3
* Unix OS

# Установка #
1) Клонировать репозиторий:

```
#!git

git clone git@bitbucket.org:gubaruba/mongosql.git
```
2) Зайти в каталог с приложением.

3) Выкачать зависимости:


```
#!cli

composer install
```

4) Дать скрипту **mongoSQL** права на запуск:


```
#!cli

chmod +x mongoSQL
```

5) Создать ссылку на скрипт **mongoSQL**, например:

```
#!cli

ln -s /path/to/script/mongoSQL /usr/local/bin/mongoSQL
```

6) При необходимости заменить конфигурационные данные для подключения в файле **config.php**

7) Импортировать тестовые данные, например:

```
#!cli

mongoimport --db test_db --collection=bios --file ./bios.json
```

# Использование #

Запросы вводятся в следующем формате (тело запроса должно быть заключено в кавычки):

```
#!cli

mongoSQL 'SELECT name, age, contacts.* FROM bios WHERE level > 1 ORDER BY age DESC, level asc, name DESC SKIP 2 LIMIT 2'
```

# Тесты #

PHPUnit должен поставиться композером, поэтому можно запускать локально:

```
#!cli

vendor/bin/phpunit tests
```