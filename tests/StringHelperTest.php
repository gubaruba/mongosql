<?php
namespace tests;

use application\helpers\StringHelper;
use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{
    public function testStringStartsWith()
    {
        $this->assertTrue(StringHelper::stringStartsWith('where age=30', 'where'));
        $this->assertFalse(StringHelper::stringStartsWith('select name', 'where'));
    }

    public function testCastType()
    {
        $this->assertSame(100, StringHelper::castType('100'));
        $this->assertSame(100.5, StringHelper::castType('100.5'));
        $this->assertSame('string', StringHelper::castType('string'));
    }
}