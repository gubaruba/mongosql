<?php
namespace tests;

use application\mysql\Query;
use PHPUnit\Framework\TestCase;

class QueryTest extends TestCase
{
    private $db;

    private $collectionName = 'bios';

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixture = [
        [
            '_id' => 1,
            'name' => 'Ivan Ivanov',
            'age' => 20,
            'level' => 1,
            'contacts' => [
                'email' => 'ivanov@example',
                'phone' => '111-11-11'
            ]
        ],
        [
            '_id' => 2,
            'name' => 'Petr Petrov',
            'age' => 20,
            'level' => 2,
            'contacts' => [
                'email' => 'petrov@example.com',
                'phone' => '222-22-22'
            ]
        ],
        [
            '_id' => 3,
            'name' => 'Sergey Sergeev',
            'age' => 25,
            'level' => 2,
            'contacts' => [
                'email' => 'sergeev@example.com',
                'phone' => '333-33-33'
            ]
        ],
        [
            '_id' => 4,
            'name' => 'Mykola Mykolaiv',
            'age' => 30,
            'level' => 1,
            'contacts' => [
                'email' => 'mykolaiv@example.com',
                'phone' => '444-44-44'
            ]
        ],
        [
            '_id' => 5,
            'name' => 'Kyrylo Kyrylov',
            'age' => 30,
            'level' => 4,
            'contacts' => [
                'email' => 'kyrylov@example.com',
                'phone' => '555-55-55'
            ]
        ],
        [
            '_id' => 6,
            'name' => 'Semen Semenov',
            'age' => 30,
            'level' => 2,
            'contacts' => [
                'email' => 'semenov@example.com',
                'phone' => '666-66-66'
            ]
        ],
        [
            '_id' => 7,
            'name' => 'Denys Denysov',
            'age' => 30,
            'level' => 2,
            'contacts' => [
                'email' => 'semenov@example.com',
                'phone' => '666-66-66'
            ]
        ],
    ];

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $config = require(__DIR__ . '/../config.php');
        $this->connection = new \MongoClient($config['server']);
        $this->collection = $this->connection->selectCollection($config['test_dbname'], $this->collectionName);
        $this->db = $this->connection->selectDB($config['test_dbname']);
    }

    private function setUpFixtures()
    {
        $this->collection->drop();
        $this->collection->batchInsert($this->fixture);
    }

    public function testExecute()
    {
        $this->setUpFixtures();

        $query = new Query();
        $all = [];
        foreach ($this->fixture as $fixture) {
            $all[$fixture['_id']] = $fixture;
        }
        $result = $query->execute($this->db, 'SELECT * FROM ' . $this->collectionName);
        $this->assertEquals($all, $result);

        $query = new Query();
        $result = $query->execute($this->db, 'SELECT name FROM ' . $this->collectionName . ' where level>1 ORDER BY age desc');
        $this->assertEquals(
            [
                2 => ['_id' => 2, 'name' => 'Petr Petrov',],
                3 => ['_id' => 3, 'name' => 'Sergey Sergeev',],
                5 => ['_id' => 5, 'name' => 'Kyrylo Kyrylov',],
                7 => ['_id' => 7, 'name' => 'Denys Denysov',],
                6 => ['_id' => 6, 'name' => 'Semen Semenov',],
            ],
            $result
        );

        $query = new Query();
        $result = $query->execute($this->db, 'SELECT name, level, age FROM ' . $this->collectionName . ' where level > 1 ORDER BY age DESC, level asc, name DESC');
        $this->assertSame(
            [
                6 => ['_id' => 6, 'name' => 'Semen Semenov', 'age' => 30, 'level' => 2,],
                7 => ['_id' => 7, 'name' => 'Denys Denysov', 'age' => 30, 'level' => 2,],
                5 => ['_id' => 5, 'name' => 'Kyrylo Kyrylov', 'age' => 30, 'level' => 4,],
                3 => ['_id' => 3, 'name' => 'Sergey Sergeev', 'age' => 25, 'level' => 2,],
                2 => ['_id' => 2, 'name' => 'Petr Petrov', 'age' => 20, 'level' => 2,],
            ],
            $result
        );

        $query = new Query();
        $result = $query->execute($this->db, 'SELECT name, level, age FROM ' . $this->collectionName . ' where level > 1 ORDER BY age DESC, level asc, name DESC SKIP 2 LIMIT 2');
        $this->assertSame(
            [
                5 => ['_id' => 5, 'name' => 'Kyrylo Kyrylov', 'age' => 30, 'level' => 4,],
                3 => ['_id' => 3, 'name' => 'Sergey Sergeev', 'age' => 25, 'level' => 2,],
            ],
            $result
        );

        $query = new Query();
        $result = $query->execute($this->db, 'SELECT * FROM ' . $this->collectionName . ' where name = "Petr Petrov"');
        $this->assertSame(
            [
                2 => [
                    '_id' => 2,
                    'name' => 'Petr Petrov',
                    'age' => 20,
                    'level' => 2,
                    'contacts' => [
                        'email' => 'petrov@example.com',
                        'phone' => '222-22-22'
                    ]
                ]
            ],
            $result
        );

        $query = new Query();
        $result = $query->execute($this->db, 'SELECT name, contacts.email FROM ' . $this->collectionName . ' where level = 2 XOR age = 30');
        $this->assertSame(
            [
                2 => [
                    '_id' => 2,
                    'name' => 'Petr Petrov',
                    'contacts' => [
                        'email' => 'petrov@example.com',
                    ]
                ],
                3 => [
                    '_id' => 3,
                    'name' => 'Sergey Sergeev',
                    'contacts' => [
                        'email' => 'sergeev@example.com',
                    ]
                ],
                4 => [
                    '_id' => 4,
                    'name' => 'Mykola Mykolaiv',
                    'contacts' => [
                        'email' => 'mykolaiv@example.com',
                    ]
                ],
                5 => [
                    '_id' => 5,
                    'name' => 'Kyrylo Kyrylov',
                    'contacts' => [
                        'email' => 'kyrylov@example.com',
                    ]
                ],
            ],
            $result
        );
    }

    /**
     * @expectedException \Exception
     */
    public function testExecuteInvalidQueryFirstException()
    {
        $db = $this->getMockBuilder(\MongoDB::class)->disableOriginalConstructor()->getMock();
        $query = new Query();
        $query->execute($db, 'Wrong SQL');
    }

    /**
     * @expectedException \Exception
     */
    public function testExecuteInvalidQuerySecondException()
    {
        $query = new Query();
        $query->execute($this->db, 'SELECT * FROM test GROUP BY level');
    }

    public function testSetProjection()
    {
        $query = new Query();

        $query->setProjection('SELECT name');
        $this->assertEquals(['name'], $query->getProjection());

        $query->setProjection('SELECT name, contacts.phone');
        $this->assertEquals(['name', 'contacts.phone'], $query->getProjection());

        $query->setProjection('SELECT name, contacts.phone, access.* ');
        $this->assertEquals(['name', 'contacts.phone', 'access'], $query->getProjection());

        $query->setProjection('SELECT access.email.* ');
        $this->assertEquals(['access.email'], $query->getProjection());

        $query->setProjection('SELECT *');
        $this->assertEquals([], $query->getProjection());
    }

    /**
     * @expectedException \Exception
     */
    public function testSetProjectionEmptyExpression()
    {
        $query = new Query();
        $query->setProjection('');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetProjectionInvalidExpression()
    {
        $query = new Query();

        $query->setProjection('Invalid select expression');
    }

    public function testSetCollection()
    {
        $query = new Query();

        $query->setCollection('FROM table');
        $this->assertEquals('table', $query->getCollection());
    }

    /**
     * @expectedException \Exception
     */
    public function testSetCollectionEmptyExpression()
    {
        $query = new Query();
        $query->setCollection('');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetCollectionInvalidExpression()
    {
        $query = new Query();
        $query->setCollection('Invalid from expression');
    }

    public function testSetCondition()
    {
        $query = new Query();

        $query->setCondition('WHERE email = test@example.com');
        $this->assertEquals(['email' => ['$eq' => 'test@example.com']], $query->getCondition());

        $query->setCondition('WHERE email = test@example.com AND age<=30');
        $this->assertEquals(['$and' => [['email' => ['$eq' => 'test@example.com']], ['age' => ['$lte' => 30]]]], $query->getCondition());

        $query->setCondition('WHERE age = 30 XOR level = 2');
        $this->assertEquals(
            [
                '$or' => [
                    [
                        '$and' => [
                            ['age' => ['$eq' => 30]],
                            [
                                '$nor' => [
                                    ['level' => ['$eq' => 2]]
                                ]
                            ]
                        ]
                    ],
                    [
                        '$and' => [
                            ['level' => ['$eq' => 2]],
                            [
                                '$nor' => [
                                    ['age' => ['$eq' => 30]]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            $query->getCondition()
        );

        $query->setCondition('WHERE email = test@example.com AND age<=30 AND level > 2');
        $this->assertEquals(
            [
                '$and' => [
                    [
                        '$and' => [
                            ['email' => ['$eq' => 'test@example.com']],
                            ['age' => ['$lte' => 30]]
                        ]
                    ],
                    [
                        'level' => ['$gt' => 2]
                    ]
                ]
            ],
            $query->getCondition()
        );

        $query->setCondition('WHERE email = test@example.com AND age<=30 XOR level <> 2 OR name="Ivan Ivanov" OR name="Petr Petrov"');
        $this->assertEquals(
            [
                '$or' => [
                    [
                        '$or' => [
                            [
                                '$or' => [
                                    [
                                        '$and' => [
                                            [
                                                '$and' => [
                                                    ['email' => ['$eq' => 'test@example.com']],
                                                    ['age' => ['$lte' => 30]]
                                                ]
                                            ],
                                            [
                                                '$nor' => [
                                                    ['level' => ['$ne' => 2]]
                                                ]
                                            ]
                                        ]
                                    ],
                                    [
                                        '$and' => [
                                            [
                                                'level' => ['$ne' => 2]
                                            ],
                                            [
                                                '$nor' => [
                                                    [
                                                        '$and' => [
                                                            ['email' => ['$eq' => 'test@example.com']],
                                                            ['age' => ['$lte' => 30]]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                ]
                            ],
                            ['name' => ['$eq' => 'Ivan Ivanov']],
                        ]
                    ],
                    [
                        'name' => ['$eq' => 'Petr Petrov'],
                    ]
                ]
            ],
            $query->getCondition()
        );
    }

    /**
     * @expectedException \Exception
     */
    public function testSetConditionEmptyExpression()
    {
        $query = new Query();
        $query->setCondition('');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetConditionInvalidExpression()
    {
        $query = new Query();
        $query->setCondition('Invalid where expression');
    }

    public function testSetSort()
    {
        $query = new Query();

        $query->setSort('ORDER BY name');
        $this->assertEquals(['name' => 1], $query->getSort());

        $query->setSort('ORDER BY name, contacts.email');
        $this->assertEquals(['name' => 1, 'contacts.email' => 1], $query->getSort());

        $query->setSort('ORDER BY name, contacts.email ASC');
        $this->assertEquals(['name' => 1, 'contacts.email' => 1], $query->getSort());

        $query->setSort('ORDER BY name, contacts.email DESC');
        $this->assertEquals(['name' => 1, 'contacts.email' => -1], $query->getSort());

        $query->setSort('ORDER BY name ASC, contacts.email');
        $this->assertEquals(['name' => 1, 'contacts.email' => 1], $query->getSort());

        $query->setSort('ORDER BY name ASC, contacts.email ASC');
        $this->assertEquals(['name' => 1, 'contacts.email' => 1], $query->getSort());

        $query->setSort('ORDER BY name ASC, contacts.email DESC');
        $this->assertEquals(['name' => 1, 'contacts.email' => -1], $query->getSort());

        $query->setSort('ORDER BY name ASC, contacts.email DESC');
        $this->assertEquals(['name' => 1, 'contacts.email' => -1], $query->getSort());

        $query->setSort('ORDER BY name DESC, contacts.email');
        $this->assertEquals(['name' => -1, 'contacts.email' => 1], $query->getSort());

        $query->setSort('ORDER BY name DESC, contacts.email ASC');
        $this->assertEquals(['name' => -1, 'contacts.email' => 1], $query->getSort());

        $query->setSort('ORDER BY name DESC, contacts.email DESC');
        $this->assertEquals(['name' => -1, 'contacts.email' => -1], $query->getSort());
    }

    /**
     * @expectedException \Exception
     */
    public function testSetSortEmptyExpression()
    {
        $query = new Query();
        $query->setSort('');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetSortInvalidExpression()
    {
        $query = new Query();
        $query->setSort('Invalid order by expression');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetGroup()
    {
        $query = new Query();
        $query->setGroup('GROUP BY name');
    }

    public function testSetSkip()
    {
        $query = new Query();

        $query->setSkip('SKIP 35');
        $this->assertEquals(35, $query->getSkip());
    }

    /**
     * @expectedException \Exception
     */
    public function testSetSkipEmptyExpression()
    {
        $query = new Query();
        $query->setSort('');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetSkipInvalidExpression()
    {
        $query = new Query();
        $query->setSort('Invalid skip expression');
    }

    public function testSetLimit()
    {
        $query = new Query();

        $query->setLimit('LIMIT 35');
        $this->assertEquals(35, $query->getLimit());
    }

    /**
     * @expectedException \Exception
     */
    public function testSetLimitEmptyExpression()
    {
        $query = new Query();
        $query->setLimit('');
    }

    /**
     * @expectedException \Exception
     */
    public function testSetLimitInvalidExpression()
    {
        $query = new Query();
        $query->setLimit('Invalid limit expression');
    }
}