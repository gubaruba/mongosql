<?php
namespace tests;

use application\mysql\Query;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;
use application\Application;

class ApplicationTest extends TestCase
{
    /**
     * @covers Application::__construct()
     */
    public function testConstructor()
    {
        $config = ['server' => 'mongodb://localhost:27017', 'dbname' => 'test_db'];
        $application = new Application($config);
        $this->assertEquals('mongodb://localhost:27017', $application->server);
        $this->assertEquals('test_db', $application->dbname);
        $this->assertInstanceOf('MongoDB', $application->getDb());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testConstructorInvalidConfig()
    {
        $application = new Application('invalid_config');
        $this->assertNull($application->server);
        $this->assertNull($application->dbname);
        $this->assertNull($application->getDb());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testConstructorConfigWithoutServer()
    {
        $application = new Application(['server' => '', 'dbname' => 'test_db']);
        $this->assertNull($application->server);
        $this->assertEquals('test_db', $application->dbname);
        $this->assertNull($application->getDb());
    }

    /**
     * @covers Application::configure()
     * @expectedException InvalidArgumentException
     */
    public function testConfigureInvalidConfig()
    {
        $application = new Application([]);
        $application->configure('invalid_config');
    }

    /**
     * @covers Application::readConfiguraqtion()
     */
    public function testConfigure()
    {
        $mock = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->setMethods(['__construct'])->getMock();
        $mock->configure(['server' => 'mongodb://localhost:27017', 'dbname' => 'test_db']);
        $this->assertEquals('mongodb://localhost:27017', $mock->server);
        $this->assertEquals('test_db', $mock->dbname);
    }

    /**
     * @covers Application::run()
     */
    public function testRun()
    {
        $application = new Application(['server' => 'mongodb://localhost:27017', 'dbname' => 'test_db']);
        $application->setQuery($this->getMockBuilder(Query::class)->setMethods(['execute'])->getMock());
        $application->getQuery()->expects($this->once())->method('execute')->willReturn(['_id' => 2, 'name' => 'Petr Petrov']);
        $this->expectOutputString("name: Petr Petrov\n");
        $application->run('SELECT name FROM bios WHERE email=petrov@example.com');
    }

    /**
     * @covers Application::run()
     */
    public function testRunError()
    {
        $application = new Application(['server' => 'mongodb://localhost:27017', 'dbname' => 'test_db']);
        $application->setQuery($this->getMockBuilder(Query::class)->setMethods(['execute'])->getMock());
        $application->getQuery()->expects($this->once())->method('execute')->will($this->throwException(new \Exception('You have an error in SQL syntax; check the right syntax to use near WHERE')));
        $this->expectOutputString("Error: You have an error in SQL syntax; check the right syntax to use near WHERE\n");
        $application->run('SELECT name FROM test WHERE wrong arguments');
    }
}